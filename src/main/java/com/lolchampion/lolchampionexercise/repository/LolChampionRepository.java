package com.lolchampion.lolchampionexercise.repository;

import com.lolchampion.lolchampionexercise.entity.LolChampion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LolChampionRepository extends JpaRepository<LolChampion, Long> {

}