package com.lolchampion.lolchampionexercise.service;

import com.lolchampion.lolchampionexercise.entity.LolChampion;
import com.lolchampion.lolchampionexercise.repository.LolChampionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LolChampionService {

    // needs a repository
    @Autowired
    LolChampionRepository lolChampionRepository;

    // method to retrieve all LolChampion objects
    public List<LolChampion> findAll() {
        return this.lolChampionRepository.findAll();
    }

    // method to save an LolChampion object
    public LolChampion save(LolChampion lolChampion) {
        return this.lolChampionRepository.save(lolChampion);
    }


}
