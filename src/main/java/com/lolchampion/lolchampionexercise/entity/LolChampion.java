package com.lolchampion.lolchampionexercise.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class LolChampion {

    // Step 1 = add entity class, add getters and setters, add toString

    // attributes
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY) // the equivalent of auto increment - don't get dups
    // @Column(name = "Id")
    private Long id;

    // @Column(name = "Name")
    private String name;

    // @Column(name = "Role")
    private String role;

    // @Column(name = "Difficulty")
    private String difficulty;

    // getters and setters
    public Long getId() {

        return id;
    }

    public void setId(Long id) {

        this.id = id;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public String getRole() {

        return role;
    }

    public void setRole(String role) {

        this.role = role;
    }

    public String getDifficulty() {

        return difficulty;
    }

    public void setDifficulty(String difficulty) {

        this.difficulty = difficulty;
    }

    // methods
    @Override
    public String toString() {
        return "LolChampion{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", role='" + role + '\'' +
                ", difficulty='" + difficulty + '\'' +
                '}';
    }
}
