package com.lolchampion.lolchampionexercise.controller;

import com.lolchampion.lolchampionexercise.entity.LolChampion;
import com.lolchampion.lolchampionexercise.service.LolChampionService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1/lolchampion")
public class LolChampionController {

    // creating a logger object
    // will create a logger for each class - make it for this class only
    private static final Logger LOG = LoggerFactory.getLogger(LolChampionController.class);

    @Autowired
    private LolChampionService lolChampionService;

    @GetMapping
    public List<LolChampion> findAll() {
        return this.lolChampionService.findAll();
    }

    @PostMapping
    public LolChampion save(@RequestBody LolChampion lolChampion) {
        // could be LOG.debug, info, warn, error or critical
        // NOTE within application.properties also need to set: logging.level.com.lolchampion.lolchampionexercise=DEBUG
        LOG.debug("Request to create lolChampion [" + lolChampion + "]");
        return lolChampionService.save(lolChampion);
    }

}
