package com.lolchampion.lolchampionexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LolchampionExerciseApplication {

    public static void main(String[] args) {

        SpringApplication.run(LolchampionExerciseApplication.class, args);
    }

}
